using UnityEngine;

public class GameInit : MonoBehaviour
{
    public void Awake()
    {
		Request.RequestManager.Instance.Init();
		Core.Flow.FlowManager.Instance.Init();
		CustomCamera.CameraManager.Instance.Init();
    }
}